import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

/**
 * Filters form service
 *
 * Allows SubCards to signal to the MainNav when a filters form is opened
 */
@Injectable({
  providedIn: 'root',
})
export class FiltersFormService {
  private openedFormIndexSource = new Subject<number>();
  openedFormIndex$ = this.openedFormIndexSource.asObservable();

  constructor() {}

  sendOpenedFormIndex(index: number) {
    this.openedFormIndexSource.next(index);
  }
}
