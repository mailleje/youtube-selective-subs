import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { FiltersFormService } from '../services/filters-form.service';
import { Subscription } from '../types/user.model';

/**
 * Subscription card component
 *
 * Displays a channel subscription and its filters.
 * Offers functionality to add/delete filters and delete the subscription.
 */
@Component({
  selector: 'app-sub-card',
  templateUrl: './sub-card.component.html',
  styleUrls: ['./sub-card.component.scss'],
})
export class SubCardComponent {
  @Input() data: Subscription;
  @Input() index: number;

  newFiltersForm = this.fb.group({
    filters: null,
  });
  formVisible = false;

  constructor(
    public auth: AuthService,
    private fb: FormBuilder,
    public filtersForm: FiltersFormService
  ) {}

  setThumbnailStyle(thumbnail: string) {
    return { backgroundImage: `url(${thumbnail})`, backgroundSize: 'cover' };
  }

  deleteSubscription() {
    this.auth.deleteSubscription(this.data.id);
  }

  deleteFilter(filter: string) {
    this.auth.deleteFilter(this.data.id, filter);
  }

  async onSubmit() {
    const filters = this.newFiltersForm.get('filters').value;
    this.auth.addFilters(this.data.id, filters);
    this.newFiltersForm.reset();
  }

  toggleFiltersForm() {
    if (!this.formVisible) this.filtersForm.sendOpenedFormIndex(this.index);
    this.formVisible = !this.formVisible;
  }

  hideFiltersForm() {
    this.formVisible = false;
  }
}
