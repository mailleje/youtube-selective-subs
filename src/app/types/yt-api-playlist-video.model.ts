interface defaultType {
  url: string;
  width: number;
  height: number;
}

interface mediumType {
  url: string;
  width: number;
  height: number;
}

interface highType {
  url: string;
  width: number;
  height: number;
}

interface standardType {
  url: string;
  width: number;
  height: number;
}

interface maxresType {
  url: string;
  width: number;
  height: number;
}

interface thumbnailsType {
  default: defaultType;
  medium: mediumType;
  high: highType;
  standard: standardType;
  maxres: maxresType;
}

interface resourceIdType {
  kind: string;
  videoId: string;
}

/**
 * Typescript version of JSON data fetched by Youtube API v3.
 * Represents a Youtube video obtained from a playlist (without statistics due to API limitations).
 */
export interface YtApiPlaylistVideo {
  publishedAt: string;
  channelId: string;
  title: string;
  description: string;
  thumbnails: thumbnailsType;
  channelTitle: string;
  playlistId: string;
  position: number;
  resourceId: resourceIdType;
}
