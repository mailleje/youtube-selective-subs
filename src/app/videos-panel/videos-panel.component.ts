import { Component } from '@angular/core';
import { YtApiVideo } from '../types/yt-api-video.model';
import { YoutubeService } from '../services/youtube.service';
import { AuthService } from '../services/auth.service';

/**
 * Videos panel component
 *
 * Handles the display of video cards.
 */
@Component({
  selector: 'app-videos-panel',
  templateUrl: './videos-panel.component.html',
  styleUrls: ['./videos-panel.component.scss'],
})
export class VideosPanelComponent {
  videos: YtApiVideo[] = [];
  private authState = false;

  constructor(
    private youtubeService: YoutubeService,
    private auth: AuthService
  ) {
    // Subscribe to auth state to automatically call getVideos once user is authenticated
    this.auth.getAuthState().subscribe((authState) => {
      this.authState = authState;
      if (this.authState == true && this.auth.apiKey$) {
        this.refreshVideos();
      }
    });

    // Subscribe to subscriptions to refresh videos when subscriptions change
    this.auth.getSubscriptionsState().subscribe(() => {
      if (this.authState == true && this.auth.apiKey$) {
        this.refreshVideos();
      }
    });
  }

  private sortVideos() {
    this.videos.sort((a, b) => {
      return (
        new Date(b.snippet.publishedAt).getTime() -
        new Date(a.snippet.publishedAt).getTime()
      );
    });
  }

  private async refreshVideos() {
    this.videos = await this.youtubeService.getVideos();
    this.sortVideos();
  }
}
